from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('feedback/', views.feedback, name='feedback'),
    path('login/', views.login_view, name='login'),
    path('registration/', views.register, name='registration'),
    path('logout/', views.logout_view, name='logout'),
    path('cabinet/', views.FileFieldView.as_view(), name='cabinet'),
    path('uploaded/', views.uploaded, name='uploaded')
]