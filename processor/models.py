from django.db import models

# Create your models here.
class Feedback(models.Model):
    text = models.TextField()
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.text

