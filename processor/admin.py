from django.contrib import admin

# Register your models here.
from processor.models import Feedback

admin.site.register(Feedback)
