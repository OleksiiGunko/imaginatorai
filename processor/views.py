import os
import shutil

import numpy as np
import uuid
import cv2 as cv
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import FormView

from processor.models import Feedback
from processor.forms import FileFieldForm

from processor.forms import RegisterForm


def index(request):
    if request.method == 'POST':
        print('index() - post received')
        loaded_photo = request.FILES['photo']

        if not loaded_photo:
            print("no photo")
        else:
            filename = add_color(loaded_photo, "unknown")
            if filename is not None:
                with open(filename, "rb") as f:
                    return HttpResponse(f.read(), content_type="image/jpeg")

    print("New request received")
    return render(request, 'processor/index.html', {})


def feedback(request):
    if request.method == 'POST':
        print('saving feedback')
        feedback = Feedback(text=request.POST.get('text', None))
        feedback.save()
        return HttpResponse(status=201)
    else:
        return HttpResponse(status=400)


def login_view(request):
    if request.POST:
        name = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=name, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect("/cabinet")
            else:
                context = {
                    "error": "Пользователь не прошел подтверждение"
                }
                return render(request, 'processor/login.html', context)
        else:
            context = {
                "error": "Пользователь не найден"
            }
            return render(request, 'processor/login.html', context)
    else:
        return render(request, "processor/login.html")


class FileFieldView(LoginRequiredMixin, FormView):
    form_class = FileFieldForm
    template_name = 'processor/cabinet.html'  # Replace with your template.
    success_url = '/uploaded/'  # Replace with your URL or reverse().
    login_url = '/login/'
    redirect_field_name = 'redirect_to'

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        files = request.FILES.getlist('file_field')
        if form.is_valid():
            for f in files:
                filename = add_color(f, request.user.id)
                if filename is None:
                    print('Error on saving file')

            results_dir = "./results/%s" % (request.user.id)
            output_filename = "./results/%s"%(request.user.id)
            shutil.make_archive(output_filename, 'zip', results_dir)

            with open(output_filename + ".zip", "rb") as f:
                print('return zip')
                return HttpResponse(f.read(), content_type='application/zip')

            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


def logout_view(request):
    logout(request)
    return redirect("/")


def register(request):
    if request.POST:
        form = RegisterForm(request.POST)
        print('form is valid')
        if form.is_valid():
            form.save()
            return redirect('/cabinet')
        else:
            print("errors", form.errors)
            return render(request, 'processor/registration.html')

    return render(request, 'processor/registration.html')

@login_required(login_url='/login')
def uploaded(request):
    return render(request, 'processor/uploaded.html')

def add_color(in_memory_photo, directory):
    try:
        in_memory_photo = in_memory_photo.read()
        nparr = np.fromstring(in_memory_photo, np.uint8)
        frame = cv.imdecode(nparr, 4)

        # using edge kernal
        sharpen_kernel = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]])
        img = cv.filter2D(frame, -1, sharpen_kernel)

        frame = cv.GaussianBlur(img, (3, 3), 0)
        # denosising
        frame = cv.fastNlMeansDenoisingColored(frame, None, 10, 10, 7, 21)


        numpy_file = np.load('./models/pts_in_hull.npy')
        Caffe_net = cv.dnn.readNetFromCaffe("models/colorization_deploy_v2.prototxt",
                                            "models/colorization_release_v2.caffemodel")

        numpy_file = numpy_file.transpose().reshape(2, 313, 1, 1)
        Caffe_net.getLayer(Caffe_net.getLayerId('class8_ab')).blobs = [numpy_file.astype(np.float32)]
        Caffe_net.getLayer(Caffe_net.getLayerId('conv8_313_rh')).blobs = [np.full([1, 313], 2.606, np.float32)]

        input_width = 224
        input_height = 224
        rgb_img = (frame[:, :, [2, 1, 0]] * 1.0 / 255).astype(np.float32)
        lab_img = cv.cvtColor(rgb_img, cv.COLOR_RGB2Lab)
        l_channel = lab_img[:, :, 0]
        l_channel_resize = cv.resize(l_channel, (input_width, input_height))
        l_channel_resize -= 50

        Caffe_net.setInput(cv.dnn.blobFromImage(l_channel_resize))
        ab_channel = Caffe_net.forward()[0, :, :, :].transpose((1, 2, 0))
        (original_height, original_width) = rgb_img.shape[:2]
        ab_channel_us = cv.resize(ab_channel, (original_width, original_height))
        lab_output = np.concatenate((l_channel[:, :, np.newaxis], ab_channel_us), axis=2)
        bgr_output = np.clip(cv.cvtColor(lab_output, cv.COLOR_Lab2BGR), 0, 1)

        files_dir = "./results/%s"%(directory)
        if not os.path.exists(files_dir):
            os.makedirs(files_dir)

        filename = "%s/%s.png" % (files_dir, str(uuid.uuid4()))

        cv.imwrite(filename, (bgr_output * 255).astype(np.uint8))

        return filename
    except Exception as err:
        print("Error occured:", err)
        return None


